#!/bin/bash
# init

## ----------------------------------
# Descrtiption du script
# Auteur: Maxime Cordeiro
#   - Aide : affiche les commandes possible
#   - Init : script d'initialisation
#   - Créer un utilisateur
#   - Suppression de l'acces root en ssh
#   - Changer le port de connection ssh
#   - Mise à jour du system
#   - Installation d'un site apache
#   - Installation d'un site wordpress
#   - Installation de php
#   - Installation de php fast cgi
#   - Configuration sur le site de de php fast cgi
#   - Instalation certbot
#   - Instalation htpassword
#   - Instalation htaccess
#   - Instalation composer
#   - Instalation cloner
#   - Utilisation memoire
#   - Utilisation CPU
#   - Nombbre de connections TCP 
#   - Version du kernel
#   - Check Complet
## ----------------------------------

## ----------------------------------
# #1: Fonction initialiser au debut du script
## ----------------------------------
function pause(){
   read -p "$*"
}

## ----------------------------------
# #2: Constantes
## ----------------------------------
server_name=$(hostname)
DIR="${PWD}"
USER_SCRIPT=$USER
args=("$@")
IP=`curl ifconfig.me/ip`

## ----------------------------------
# #3: Constantes couleurs
## ----------------------------------
GREEN='\e[32m'
BLUE='\e[34m'
NOCOLOR='\e[0;m'
RED='\033[0;41;30m'
STD='\033[0;0;39m'

## ----------------------------------
# #4: Fonction couleurs
## ----------------------------------

ColorGreen(){
	echo -ne $GREEN$1$NOCOLOR
}

ColorBlue(){
	echo -ne $BLUE$1$NOCOLOR
}

## ----------------------------------
# #5: Fonctions Principales
# ----------------------------------

## ----------------------------------
# Fonction : parse_options
# permet l'utilisation d'argument ex: ./initscript.sh -h
# si aucun argument on lance le menu interactif
## ----------------------------------
parse_options() {
  case ${args[0]} in
    -h|--help)
      help_list
      exit
      ;;
    -a|--apache)
      apache
      ;;
    -u|--user)
      create_user
      ;;
    -cl|--cloner)
      cloner
      ;;
    -i|--ip)
      ip
      ;;
    -in|--init)
      init
      ;;
    -l|--lamp)
      lamp
      ;;
    -ma|--mariadb)
      mariadb
      ;;
    -sy|--symfony)
      symfony
      ;;
    -w|--wordpress)
      wordpress
      ;;
    -sr|--suproot)
      suprootssh
      ;;
    -cp|--chport)
      changeport
      ;;
    -cp|--chport)
      changeport
      ;;
    -ssh|--cossh)
      connexionssh
      ;;
    -up|--update)
      update
      ;;
    -pi|--phpinstal)
      installphp
      ;;
    -cfp|--confphp)
      configphp
      ;;
    -cb|--certbot)
      certbot
      ;;
    -ps|--confsitephp)
      confsitephp
      ;;
    -ht|--htaccess)
      htaccess
      ;;
    -comp|--composer)
      composer
      ;;
    *)
      # APPELLE DU MENU
      menu
  esac
}

# -----------------------------------
# Menu - Boucle infini
# ------------------------------------
menu(){
clear
while true
do
	show_menus
	read_options
done
}

# -----------------------------------
# Menu - Affichage
# ------------------------------------
show_menus(){
  clear
  echo "
            ~~~~~~~~~~~~~~~~~~~~~	
             - - - M E N U - - -
            ~~~~~~~~~~~~~~~~~~~~~
 ______________________________________________
 |                                            |
 |              .,-:;//;:=,                   |
 |            . :H@@@MM@M#H/.,+%;,            |
 |         ,/X+ +M@@M@MM%=,-%HMMM@X/,         |
 |       -+@MM; MM@@MH+-,;XMMMM@MMMM@+-       |
 |      ;@M@@M- XM@X;. -+XXXXXHHH@M@M#@/.     |
 |    ,%MM@@MH ,@%=            .---=-=:=,.    |
 |    =@#@@@MX .,              -%HXMM%%%+;    |
 |   =-./@M@M$                  .;@MMMM@MM:   |
 |   X@/ -MMM/                    .+MM@@@M$   |
 |  ,@M@H: :@:                    . =X#@@@@-  |
 |  ,@@@MMX, .                    /H- ;@M@M=  |
 |  .H@@@@M@+,                    %MM+..%#$.  |
 |   /MMMM@MMH/.                  XM@MH; =;   |
 |    /%+%MXHH@$=              , .H@@@@MX,    |
 |     .=--------.           -%H.,@@@@@MX,    |
 |     .%MM@@@HHHXXMMM%+- .:MMMX =M@@MM%.     |
 |       =XMMM@MM@MM#H;,-+HMM@M+ /MMMX=       |
 |         =%@M@M#@M-.=M@MM@@@M; %M%=         |
 |           ,:+$+-,/H#MMMMMMM@= =,           |
 |                 =++%%%%+/:-.               |
 |____________________________________________|

  $(ColorGreen '1)') Aide : affiche les commandes possible
  $(ColorGreen '2)') Init : script initialisation
  $(ColorGreen '3)') Créer un utilisateur
  $(ColorGreen '4)') Suppression de acces root en ssh
  $(ColorGreen '5)') Changer le port de connection ssh
  $(ColorGreen '6)') Mise à jour du system
  $(ColorGreen '7)') Installation site apache
  $(ColorGreen '8)') Installation site wordpress
  $(ColorGreen '9)') Installation php
  $(ColorGreen '10)') Installation php fast cgi
  $(ColorGreen '11)') Configuration sur le site de php fast cgi
  $(ColorGreen '12)') Instalation certbot
  $(ColorGreen '13)') Instalation htpassword
  $(ColorGreen '14)') Instalation htaccess
  $(ColorGreen '15)') Instalation composer
  $(ColorGreen '16)') Instalation cloner
  $(ColorGreen '17)') Utilisation mémoire
  $(ColorGreen '18)') Utilisation CPU
  $(ColorGreen '19)') Nombre de connection TCP
  $(ColorGreen '20)') Kernel
  $(ColorGreen '21)') Check Complet
  $(ColorGreen '0)') Quitter"
}

# -----------------------------------
# Menu - Choix
# ------------------------------------
read_options(){
 local option
	  read -p "$(ColorBlue 'Choisir une option:') " option
    case $option in
	    1) help_list ; menu ;;
      2) init ; menu ;;
	    3) create_user ; menu ;;
      4) suprootssh ; menu ;;
	    5) changeport ; menu ;;
	    6) update ; menu ;;
      7) apache ; menu ;;
      8) wordpress ; menu ;;
      9) installphp ; menu ;;
      10) fcgiphp ; menu ;;
      11) fcgisitephp ; menu ;;
      12) certbot ; menu ;;
      13) htpassword ; menu ;;
      14) htaccess ; menu ;;
      15) composer ; menu ;;
      16) cloner ; menu ;;
	    17) memory_check ; menu ;;
	    18) cpu_check ; menu ;;
	    19) tcp_check ; menu ;;
	    20) kernel_check ; menu ;;
	    21) all_checks ; menu ;;
		  0) exit 0 ;;
		  *) echo -e "${RED}Mauvaise option...${STD}" && sleep 1;  WrongCommand;;
    esac
}

# -----------------------------------
# Menu - Aide
# ------------------------------------
help_list() {
  clear
  echo "Utilisation

  Lancer ./${0##*/} sans arguments pour passer par le menu intercatif
  ou 
  ./${0##*/} [-h]
  ou
  ./${0##*/} [--help]

  Options:

    -h, --help
      affiche les commandes possible

    -a, --apapche
      creer un site apache
      
    -c, --cloner
      clone un depot git 

    -i, --ip
      donne l'addresse ip de la machine

    -in, --init
      script d'initialisation
    
    -l, --lamp
      crée un serveur lamp

    -ma, --mariadb
      install mariadb

    -re, --reddis
      install reddis

    -u, --user 
      créer un utilisateur
    
    -w, --wordpress
      creer un site wordpress
    "
    pause 'Presse [Entrer] pour continuer...'
}

# -----------------------------------
# #6: Les Fonctions du script
# ------------------------------------

function memory_check() {
    echo ""
	echo "L'utilisation mémoire sur ${server_name} est: "
	free -h
	echo ""
}

function cpu_check() {
    echo ""
	echo "Le CPU sur ${server_name} est: "
    echo ""
	uptime
    echo ""
}

function tcp_check() {
    echo ""
	echo "Les connections TCP sur ${server_name}: "
    echo ""
	cat  /proc/net/tcp | wc -l
    echo ""
}

function kernel_check() {
    echo ""
	echo "Le Kernel sur ${server_name} est: "
	echo ""
	uname -r
    echo ""
}

function all_checks() {
	memory_check
	cpu_check
	tcp_check
	kernel_check
}

function create_user(){
    #CREATION USER
    clear
    echo
    echo "Voulez vous créer un utilisateur ? : si oui répondre avec la touche 'Y' "
    read choice
    if [[ "$choice" ==  [yY] ]]; then
        echo
        echo " Entrez un nom de user ex : maxime" 
        choice=n
        while [[ "$choice" ==  [nN] ]]
        do
            read user
            clear
            echo
            echo " C'est ça ? si vous voulez changer le user, répondre avec la touche 'N' "
            echo "nom de user : $user "
            read choice
        done
        echo
        echo " Entrez des groupes pour cette uttilisateur ex : root,sudo,adm " 
        choice=n
        while [[ "$choice" ==  [nN] ]]
        do
            read groupes
            clear
            echo
            echo " C'est ça ? si vous voulez changer les groupes, répondre avec la touche 'N' "
            echo "groupes : $groupes "
            read choice
        done
        adduser $user
        usermod -aG $groupes $user
        clear
        echo
        echo "un utilisateur à était créer avec les commandes :"
        echo
        echo "
        adduser $user
        usermod -aG $groupes $user"
        pause 'Presse [Entrer] pour continuer...'
    fi
}

function suprootssh(){
    #SUPPRESSION DU ROOT LOGIN
    clear
    echo
    echo "Voulez vous supprimer l'acces a root au ssh ? : si oui répondre avec la touche 'Y' "
    read choice
    if [[ "$choice" ==  [yY] ]]; then
        sed -i 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
        service ssh restart
        clear
        echo
        echo "La supression à était éffectuer avec les commandes :"
        echo
        echo "
        sed -i 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
        service ssh restart"
        echo
        pause 'Presse [Entrer] pour continuer...'
    fi
}

function changeport(){
    #CHANGEMENT DE PORT
    clear
    echo
    echo "Voulez vous changer de port ? : si oui répondre avec la touche 'Y' "
    read choice
    if [[ "$choice" ==  [yY] ]]; then
        echo
        echo " Entrez un numero de port ex : 2222"
        choice=n
        while [[ "$choice" ==  [nN] ]]
        do
            read port
            clear
            echo
            echo " C'est ça ? si vous voulez changer le port, répondre avec la touche 'N' "
            echo "numero de port : $port "
            read choice
        done
        sed -i "s/#Port 22/Port $port/g" /etc/ssh/sshd_config
        service ssh restart
        clear
        echo
        echo "Le port à était changer avec les commandes :"
        echo "
        sed -i \"s/#Port 22/Port $port/g\" /etc/ssh/sshd_config
        service ssh restart"
        pause 'Presse [Entrer] pour continuer...'
    fi
}

function connexionssh(){
    #CONNEXION SSH
    clear
    echo
    echo "Voulez vous une connection sécurisée en ssh ? : si oui répondre avec la touche 'Y' "
    read choice
    if [[ "$choice" ==  [yY] ]]; then
        clear
        echo
        echo "collez ici la clé publique id_rsa.pub"
        choice=n
        while [[ "$choice" ==  [nN] ]]
        do
            read cle_pub
            clear
            echo
            echo " C'est ça ? si vous voulez changer la clé publique, répondre avec la touche 'N' "
            echo "clé publique : $cle_pub "
            read choice
        done
        clear
        if [ -z "$user" ]; then
            echo "Pour quel utilisateur ? "
            choice=n
            while [[ "$choice" ==  [nN] ]]
            do
                read user
                clear
                echo
                echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
                echo "nom de'utilisateur : $user "
                read choice
            done
        fi
        mkdir /home/$user/.ssh
        echo "$cle_pub" > /home/$user/.ssh/authorized_keys
        chmod 644 /home/$user/.ssh/authorized_keys
        cat .ssh/authorized_keys
        sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/g' /etc/ssh/sshd_config
        sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
        service ssh restart
        clear
        echo "La connection sécurisée en ssh à était effectuer avec les commandes :"
        echo
        echo "
        mkdir /home/$user/.ssh
        echo \"$cle_pub\" > /home/$user/.ssh/authorized_keys
        chmod 644 /home/$user/.ssh/authorized_keys
        cat .ssh/authorized_keys
        sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/g' /etc/ssh/sshd_config
        sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
        service ssh restart"
        echo
        pause 'Presse [Entrer] pour continuer...'
    fi
}

function update(){
    #MISE A JOUR
    clear
    echo
    echo "Voulez vous faire les mise à jour ? : si oui répondre avec la touche 'Y' "
    read choice
    if [[ "$choice" ==  [yY] ]]; then
        apt update && apt upgrade -y
        clear
        echo "La mise à jour à était effectuer avec la commande suivante :"
        echo
        echo "apt update && apt upgrade -y"
        echo
        pause 'Presse [Entrer] pour continuer...'
    fi
}

function wordpress(){
        #wordpress
        clear
        echo
        echo "Voulez vous installer un site wordpress ? : si oui répondre avec la touche 'Y' "
        read choice
        if [[ "$choice" ==  [yY] ]]; then
          clear
          echo
          echo " Entrez un nom de domaine ex : maxime-cordeiro.tk"
          choice=n
          while [[ "$choice" ==  [nN] ]]
          do
              read domain
              clear
              echo
              echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
              echo "nom de domaine : $domain "
              read choice
          done
            echo "téléchargement wordpress"
            cd /var/www && wget https://wordpress.org/latest.tar.gz && tar xfz latest.tar.gz && rm latest.tar.gz && mv wordpress $domain
            echo "<VirtualHost *:80>
            ServerName $domain

            ServerAdmin webmaster@localhost
            DocumentRoot /var/www/$domain

            
            #<Directory /var/www/$domain/>
            #  AuthType Basic
            #  AuthName \"Restricted Content\"
            #  AuthUserFile /var/www/$domain/.htpasswd
            #  Require valid-user
            #  Options FollowSymLinks
            #  AllowOverride All
            #  Require all granted
            #</Directory>

            #<Proxy *>
            #  Order deny,allow
            #  Allow from all
            #  Authtype Basic
            #  Authname \"Password Required\"
            #  AuthUserFile /var/www/$domain/.htpasswd
            #  Require valid-user
            #</Proxy>

            ErrorLog \${APACHE_LOG_DIR}/error.log
            CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

<VirtualHost *:80>
                ServerName www.$domain
                RedirectPermanent / http://$domain
</VirtualHost> " > /etc/apache2/sites-available/$domain.conf
            clear
            echo "Wordpress à était installer avec les commandes :"
            echo
            echo "
            cd /var/www && wget https://wordpress.org/latest.tar.gz && tar xfz latest.tar.gz && rm latest.tar.gz && mv wordpress $domain"
            echo
            echo "et les configuration suivantes"
            echo
            cat /etc/apache2/sites-available/$domain.conf
            echo
            pause 'Presse [Entrer] pour continuer...'
            clear
            echo
            echo "Voulez vous installer et configurer mariadb pour wordpress ? : si oui répondre avec la touche 'Y' "
            read choice
            if [[ "$choice" ==  [yY] ]]; then
                apt install php7.4-mysql mariadb-server -y
                clear
                echo
                echo "quel nom de la base de données ? "
                read dbName
                echo
                echo "quel nom d'utilisateur de la base de données ? "
                read dbUser
                echo
                echo
                echo "quel mot de passe de la base de données ? "
                read pwd
                clear
                echo
                echo "copier les commande suivantes"
                echo
                echo " CREATE DATABASE $dbName;"
                echo " SHOW DATABASES;"
                echo " CREATE USER '$dbUser'@'localhost' IDENTIFIED BY '$pwd';"
                echo " GRANT ALL PRIVILEGES ON wordpress.* TO '$dbUser'@'localhost';"
                echo
                echo "pour sortir faire : "
                echo " exit"
                echo
                echo "pour se connecter en tant que wordpress faire : "
                echo " mariadb -u $dbUser -p"
                mariadb
                clear
                echo
                echo "Mariadb à était installer avec les commandes :"
                echo
                echo "apt install php7.4-mysql mariadb-server -y"
                echo 
                echo " CREATE DATABASE $dbName;"
                echo " SHOW DATABASES;"
                echo " CREATE USER '$dbUser'@'localhost' IDENTIFIED BY '$pwd';"
                echo " GRANT ALL PRIVILEGES ON wordpress.* TO '$dbUser'@'localhost';"
                echo
                echo "et la configuration suivantes"
                echo "nom base de donnée : $dbName;"
                echo "utilisateur : $dbUser"
                echo "mot de passe : $pwd"
                echo
                pause 'Presse [Entrer] pour continuer...'
            fi
            if [ -z "$user" ]; then
                echo "Pour quel utilisateur ? "
                choice=n
                while [[ "$choice" ==  [nN] ]]
                do
                    read user
                    clear
                    echo
                    echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
                    echo "nom d'utilisateur' : $user "
                    read choice
                done
            fi
            sed -i "s/user = www-data/user = $user/g" /etc/php/7.4/fpm/pool.d/www.conf
            sed -i "s/group = www-data/group = $user/g" /etc/php/7.4/fpm/pool.d/www.conf
            clear
            echo
            echo "si l'utilisateur est par default dans /etc/php/7.4/fpm/pool.d/www.conf il à était changer avec les commande suivantes :"
            echo
            echo "
            sed -i \"s/user = www-data/user = $user/g\" /etc/php/7.4/fpm/pool.d/www.conf
            sed -i \"s/group = www-data/group = $user/g\" /etc/php/7.4/fpm/pool.d/www.conf"
            echo
            pause 'Presse [Entrer] pour continuer...'
        sudo chown -R $user:$user /var/www/$domain
        echo "Activation du site  $domain"
        a2ensite $domain
        echo "Redémarage du service apache"
        service apache2 restart
        clear
        echo
        echo "les droit du dossier ont était modifier :"
        echo
        echo "sudo chown -R $user:$user /var/www/$domain"
        echo
        echo "
        Le site à était activer : a2ensite $domain
        Le Redémarage du service apache à était effectuer : service apache2 restart"
        echo
        pause 'Presse [Entrer] pour continuer...'
    fi
}

function apache(){
    # apache et virtualhost
    clear
    echo
    echo "Voulez vous créer un site avec apache ? : si oui répondre avec la touche 'Y' "
    read choice
    if [[ "$choice" ==  [yY] ]]; then
        apt update && apt install apache2 -y
        clear
        echo "Apache à était installer avec la commande suivante :"
        echo
        echo "apt update && apt install apache2 -y"
        echo
        pause 'Presse [Entrer] pour continuer...'
        clear
        echo
        echo " Entrez un nom de domaine ex : maxime-cordeiro.tk"
        choice=n
        while [[ "$choice" ==  [nN] ]]
        do
            read domain
            clear
            echo
            echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
            echo "nom de domaine : $domain "
            read choice
        done
        echo
        echo "Voulez vous ajouter un www ? : si oui répondre avec la touche 'Y' "
        read choice
            if [[ "$choice" ==  [yY] ]]; then
            echo "<VirtualHost *:80>
                ServerName $domain

                ServerAdmin webmaster@localhost
                DocumentRoot /var/www/$domain

                
                #<Directory /var/www/$domain/>
                #  AuthType Basic
                #  AuthName \"Restricted Content\"
                #  AuthUserFile /var/www/$domain/.htpasswd
                #  Require valid-user
                #  Options FollowSymLinks
                #  AllowOverride All
                #  Require all granted
                #</Directory>

                #<Proxy *>
                #  Order deny,allow
                #  Allow from all
                #  Authtype Basic
                #  Authname \"Password Required\"
                #  AuthUserFile /var/www/$domain/.htpasswd
                #  Require valid-user
                #</Proxy>

                ErrorLog \${APACHE_LOG_DIR}/error.log
                CustomLog \${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>

    <VirtualHost *:80>
                    ServerName www.$domain
                    RedirectPermanent / http://$domain
    </VirtualHost> " > /etc/apache2/sites-available/$domain.conf
            clear
            echo
            echo "la configuratin suivante à était créer :"
            echo
            cat /etc/apache2/sites-available/$domain.conf
            echo
            echo
            pause 'Presse [Entrer] pour continuer...'
            else
            echo "<VirtualHost *:80>
                ServerName $domain

                ServerAdmin webmaster@localhost
                DocumentRoot /var/www/$domain
                
                #<Directory /var/www/$domain/>
                #  AuthType Basic
                #  AuthName \"Restricted Content\"
                #  AuthUserFile /var/www/$domain/.htpasswd
                #  Require valid-user
                #  Options FollowSymLinks
                #  AllowOverride All
                #  Require all granted
                #</Directory>

                #<Proxy *>
                #  Order deny,allow
                #  Allow from all
                #  Authtype Basic
                #  Authname \"Password Required\"
                #  AuthUserFile /var/www/$domain/.htpasswd
                #  Require valid-user
                #</Proxy>

                ErrorLog \${APACHE_LOG_DIR}/error.log
                CustomLog \${APACHE_LOG_DIR}/access.log combined
    </VirtualHost> " > /etc/apache2/sites-available/$domain.conf
            clear
            echo
            echo "la configuratin suivante à était créer :"
            echo
            cat /etc/apache2/sites-available/$domain.conf
            echo
            pause 'Presse [Entrer] pour continuer...'
            fi
            mkdir /var/www/$domain
            echo "<h1>Hello World </h1>" > /var/www/$domain/index.html
            clear
            echo
            echo "le fichier index.html et le doisser $domain ont étaient créer : "
            echo
            echo "mkdir /var/www/$domain
            echo \"<h1>Hello World </h1>\" > /var/www/$domain/index.html"
            echo
            cat /var/www/$domain/index.html
            echo
            pause 'Presse [Entrer] pour continuer...'
        sudo chown -R $user:$user /var/www/$domain
        a2ensite $domain
        service apache2 restart
        clear
        echo
        echo "les droit du dossier ont était modifier :"
        echo
        echo "sudo chown -R $user:$user /var/www/$domain"
        echo
        echo "
        Le site à était activer : a2ensite $domain
        Le Redémarage du service apache à était effectuer : service apache2 restart"
        echo
        pause 'Presse [Entrer] pour continuer...'
    fi
}

function installphp(){
    #INSTALLATION PHP
    clear
    echo
    echo "Voulez vous installer php ? : si oui répondre avec la touche 'Y' "
    read choice
    if [[ "$choice" ==  [yY] ]]; then
        apt update && apt install php -y
        clear
        echo
        echo "Voulez vous tester ? : si oui répondre avec la touche 'Y' "
        echo "vous pourrez tester avec l'url http://$domain/test.php"
        if [ -z "$domain" ]; then
            echo "Pour quel domaine ou sous-domaine ? "
            read domain
        fi
        read choice
        if [[ "$choice" ==  [yY] ]]; then
            echo "<?php phpinfo();"  > /var/www/$domain/test.php
        fi
        clear
        echo
        echo "Apache à était insaller avec la commande :"
        echo
        echo "apt update && apt install php -y"
        echo
        echo "le fichier test.php à était créer :"
        echo
        cat /var/www/$domain/test.php
        echo
        echo "vous pourrez tester avec l'url http://$domain/test.php"
        echo
        pause 'Presse [Entrer] pour continuer...'
    fi
}

function fcgiphp(){
    #ICONFIGURATION PHP
    clear
    echo
    echo "Voulez vous configurer php avec fcgi ? : si oui répondre avec la touche 'Y' "
    read choice
    if [[ "$choice" ==  [yY] ]]; then
        apt update && apt install php php-fpm -y
        a2enmod proxy
        a2enmod proxy_fcgi
        sed -i 's/listen = \/run\/php\/php7.4-fpm.sock/;listen = \/run\/php\/php7.4-fpm.sock/g' /etc/php/7.4/fpm/pool.d/www.conf
        sed -i '/;listen = \/run\/php\/php7.4-fpm.sock/a \
listen = 127.0.0.1:9000 \
listen.allowed_clients = 127.0.0.1' /etc/php/7.4/fpm/pool.d/www.conf
        a2enmod http2
        a2dismod php7.4
        a2dismod mpm_prefork
        a2enmod mpm_event
        clear
        echo
        echo "les commandes suivantes on était réaliser :"
        echo
        echo "
        insatallation php-fpm :
        apt update && apt install php-fpm -y

        Activation des modules apache2 : proxy,proxy_fcgi :
        a2enmod proxy
        a2enmod proxy_fcgi

        configuration de /etc/php/7.4/fpm/pool.d/www.conf :
        sed -i 's/listen = \/run\/php\/php7.4-fpm.sock/;listen = \/run\/php\/php7.4-fpm.sock/g' /etc/php/7.4/fpm/pool.d/www.conf
        sed -i '/;listen = \/run\/php\/php7.4-fpm.sock/a \
listen = 127.0.0.1:9000 \
listen.allowed_clients = 127.0.0.1' /etc/php/7.4/fpm/pool.d/www.conf

        Activation des modules apache2 : http2, mpm_event :
        a2enmod http2
        a2enmod mpm_event

        Désactivation des modules apache2 : php7.4, mpm_prefork :
        a2dismod php7.4
        a2dismod mpm_prefork
        "
        echo
        pause 'Presse [Entrer] pour continuer...'
        if [ -z "$user" ]; then
            echo "Pour quel utilisateur ? "
            choice=n
            while [[ "$choice" ==  [nN] ]]
            do
                read user
                clear
                echo
                echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
                echo "nom d'utilisateur : $user "
                read choice
            done
        fi
        sed -i "s/user = www-data/user = $user/g" /etc/php/7.4/fpm/pool.d/www.conf
        sed -i "s/group = www-data/group = $user/g" /etc/php/7.4/fpm/pool.d/www.conf
        service apache2 restart
        service php7.4-fpm restart
        clear
        echo "Les commande suivantes ont était réaliser :"
        echo
        echo "
        si l'utilisateur est par default dans /etc/php/7.4/fpm/pool.d/www.conf il à était changer avec les commande suivantes :
        sed -i \"s/user = www-data/user = $user/g\" /etc/php/7.4/fpm/pool.d/www.conf
        sed -i \"s/group = www-data/group = $user/g\" /etc/php/7.4/fpm/pool.d/www.conf

        Redémarage du service apache :
        service apache2 restart
        service php7.4-fpm restart"
        echo
        pause 'Presse [Entrer] pour continuer...'
    fi
}

function certbot(){
     #CERTBOT
    echo
    clear
    echo
    echo "Voulez vous créer un certificat certbot ? : si oui répondre avec la touche 'Y' "
    echo
    echo "Avant dutiliser cerbot verrifier que votre dns est bien configurer"
    echo "sinon il faudra relancer la commande : certbot --apache"
    echo "l'adresse ip de cette machine : $IP"
    read choice
    if [[ "$choice" ==  [yY] ]]; then
        apt update && apt install certbot python3-certbot-apache -y
        certbot --apache
    fi
}

function fcgisitephp(){
    #confsitephp
        clear
        echo
        echo "Voulez vous configurer php fcgi pour un site ? : si oui répondre avec la touche 'Y' "
        read choice
        if [[ "$choice" ==  [yY] ]]; then
            if [ -z "$domain" ]; then
                echo "Pour quel domaine ou sous-domaine ? "
                choice=n
                while [[ "$choice" ==  [nN] ]]
                do
                    read domain
                    clear
                    echo
                    echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
                    echo "nom de domaine : $domain "
                    read choice
                done
            fi
            sed -i "/DocumentRoot \/var\/www\/$domain/a \
            ProxyPassMatch ^/(.*\.php)$ fcgi://127.0.0.1:9000/var/www/$domain/\$1" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/DocumentRoot \/var\/www\/$domain/a \
            ProxyPassMatch ^/(.*\.php)$ fcgi://127.0.0.1:9000/var/www/$domain/\$1" /etc/apache2/sites-available/$domain.conf
        clear
        echo
        echo "Les commandes suivantes on était effectuées :"
        echo
        echo "
            apt update && apt install certbot python3-certbot-apache -y
            certbot --apache
            sed -i \"/DocumentRoot \/var\/www\/$domain/a \
            ProxyPassMatch ^/(.*\.php)$ fcgi://127.0.0.1:9000/var/www/$domain/\$1\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/DocumentRoot \/var\/www\/$domain/a \
            ProxyPassMatch ^/(.*\.php)$ fcgi://127.0.0.1:9000/var/www/$domain/\$1\" /etc/apache2/sites-available/$domain.conf"
        echo
        pause 'Presse [Entrer] pour continuer...'
        fi
}

function htpassword(){
  #HTPASSWD
  clear
  echo
  echo "Voulez vous configurer un htpasswd ? : si oui répondre avec la touche 'Y' "
  read choice
  if [[ "$choice" ==  [yY] ]]; then
    if [ -z "$domain" ]; then
      echo "Pour quel domaine ou sous-domaine ? "
      choice=n
      while [[ "$choice" ==  [nN] ]]
      do
        read domain
        clear
        echo
        echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
        echo "nom d'utilisateur : $domain "
        read choice
      done
    fi
    sed -i "/#<Directory /var/www/$domain/>/a\<Directory /var/www/$domain/>" /etc/apache2/sites-available/$domain.conf
            sed -i "/#  AuthType Basic/a\  AuthType Basic" /etc/apache2/sites-available/$domain.conf
            sed -i "/#  AuthName \"Restricted Content\"/a\  AuthName \"Restricted Content\"" /etc/apache2/sites-available/$domain.conf
            sed -i "/#  AuthUserFile /var/www/$domain/.htpasswd/a\  AuthUserFile /var/www/$domain/.htpasswd" /etc/apache2/sites-available/$domain.conf
            sed -i "/#  Require valid-user/a\  Require valid-user" /etc/apache2/sites-available/$domain.conf
            sed -i "/#</Directory>/a\</Directory>" /etc/apache2/sites-available/$domain.conf

            sed -i "/#<Proxy *>/a\<Proxy *>" /etc/apache2/sites-available/$domain.conf
            sed -i "/#  Order deny,allow/a\  Order deny,allow" /etc/apache2/sites-available/$domain.conf
            sed -i "/#  Allow from all/a\  Allow from all" /etc/apache2/sites-available/$domain.conf
            sed -i "/#  Authtype Basic/a\  Authtype Basic" /etc/apache2/sites-available/$domain.conf
            sed -i "/#  Authname \"Password Required\"/a\  Authname \"Password Required\"" /etc/apache2/sites-available/$domain.conf
            sed -i "/#  AuthUserFile /var/www/$domain/.htpasswd/a\  AuthUserFile /var/www/$domain/.htpasswd" /etc/apache2/sites-available/$domain.conf
            sed -i "/#  Require valid-user/a\  Require valid-user" /etc/apache2/sites-available/$domain.conf
            sed -i "/#</Proxy>/a\#</Proxy>" /etc/apache2/sites-available/$domain.conf
            
            sed -i "/#<Directory /var/www/$domain/>/a\<Directory /var/www/$domain/>" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#  AuthType Basic/a\  AuthType Basic" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#  AuthName \"Restricted Content\"/a\  AuthName \"Restricted Content\"" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#  AuthUserFile /var/www/$domain/.htpasswd/a\  AuthUserFile /var/www/$domain/.htpasswd" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#  Require valid-user/a\  Require valid-user" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#</Directory>/a\</Directory>" /etc/apache2/sites-available/$domain-le-ssl.conf

            sed -i "/#<Proxy *>/a\<Proxy *>" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#  Order deny,allow/a\  Order deny,allow" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#  Allow from all/a\  Allow from all" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#  Authtype Basic/a\  Authtype Basic" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#  Authname \"Password Required\"/a\  Authname \"Password Required\"" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#  AuthUserFile /var/www/$domain/.htpasswd/a\  AuthUserFile /var/www/$domain/.htpasswd" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#  Require valid-user/a\  Require valid-user" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i "/#</Proxy>/a\#</Proxy>" /etc/apache2/sites-available/$domain-le-ssl.conf
                if [ -z "$user" ]; then
                        echo "Pour quel utilisateur ? "
                        choice=n
                        while [[ "$choice" ==  [nN] ]]
                        do
                            read user
                            clear
                            echo
                            echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
                            echo "nom d'utilisateur : $user "
                            read choice
                        done
                fi
            htpasswd -c /var/www/$domain/.htpasswd $user
            service apache2 restart
            clear
            echo "Les commandes suivantes ont étaient effectuées : "
            echo
            echo "
            sed -i \"/#<Directory /var/www/$domain/>/a\<Directory /var/www/$domain/>\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#  AuthType Basic/a\  AuthType Basic\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#  AuthName \"Restricted Content\"/a\  AuthName \"Restricted Content\"\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#  AuthUserFile /var/www/$domain/.htpasswd/a\  AuthUserFile /var/www/$domain/.htpasswd\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#  Require valid-user/a\  Require valid-user\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#</Directory>/a\</Directory>\" /etc/apache2/sites-available/$domain.conf

            sed -i \"/#<Proxy *>/a\<Proxy *>\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#  Order deny,allow/a\  Order deny,allow\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#  Allow from all/a\  Allow from all\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#  Authtype Basic/a\  Authtype Basic\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#  Authname \"Password Required\"/a\  Authname \"Password Required\"\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#  AuthUserFile /var/www/$domain/.htpasswd/a\  AuthUserFile /var/www/$domain/.htpasswd\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#  Require valid-user/a\  Require valid-user\" /etc/apache2/sites-available/$domain.conf
            sed -i \"/#</Proxy>/a\#</Proxy>\" /etc/apache2/sites-available/$domain.conf
            
            sed -i \"/#<Directory /var/www/$domain/>/a\<Directory /var/www/$domain/>\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#  AuthType Basic/a\  AuthType Basic\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#  AuthName \"Restricted Content\"/a\  AuthName \"Restricted Content\"\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#  AuthUserFile /var/www/$domain/.htpasswd/a\  AuthUserFile /var/www/$domain/.htpasswd\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#  Require valid-user/a\  Require valid-user\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#</Directory>/a\</Directory>\" /etc/apache2/sites-available/$domain-le-ssl.conf

            sed -i \"/#<Proxy *>/a\<Proxy *>\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#  Order deny,allow/a\  Order deny,allow\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#  Allow from all/a\  Allow from all\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#  Authtype Basic/a\  Authtype Basic\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#  Authname \"Password Required\"/a\  Authname \"Password Required\"\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#  AuthUserFile /var/www/$domain/.htpasswd/a\  AuthUserFile /var/www/$domain/.htpasswd\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#  Require valid-user/a\  Require valid-user\" /etc/apache2/sites-available/$domain-le-ssl.conf
            sed -i \"/#</Proxy>/a\#</Proxy>\" /etc/apache2/sites-available/$domain-le-ssl.conf

            htpasswd -c /var/www/$domain/.htpasswd $user
            service apache2 restart"
            echo
            pause 'Presse [Entrer] pour continuer...'
  fi
}

function htaccess(){
  #HTACCESS
  echo "Voulez vous configurer un htacces ? : si oui répondre avec la touche 'Y' "
            read choice
            if [[ "$choice" ==  [yY] ]]; then
                sed -i "/#<Directory /var/www/$domain/>/a\<Directory /var/www/$domain/>" /etc/apache2/sites-available/$domain.conf
                sed -i "#  Options FollowSymLinks/a\Options FollowSymLinks" /etc/apache2/sites-available/$domain.conf
                sed -i "#  AllowOverride All/a\AllowOverride All" /etc/apache2/sites-available/$domain.conf
                sed -i "#  Require all granted/a\Require all granted" /etc/apache2/sites-available/$domain.conf
                sed -i "/#</Directory>/a\</Directory>" /etc/apache2/sites-available/$domain.conf

                sed -i "/#<Directory /var/www/$domain/>/a\<Directory /var/www/$domain/>" /etc/apache2/sites-available/$domain-le-ssl.conf
                sed -i "#  Options FollowSymLinks/a\Options FollowSymLinks" /etc/apache2/sites-available/$domain-le-ssl.conf
                sed -i "#  AllowOverride All/a\AllowOverride All" /etc/apache2/sites-available/$domain-le-ssl.conf
                sed -i "#  Require all granted/a\Require all granted" /etc/apache2/sites-available/$domain-le-ssl.conf
                sed -i "/#</Directory>/a\</Directory>" /etc/apache2/sites-available/$domain-le-ssl.conf

                echo '
                <IfModule mod_rewrite.c>
                RewriteEngine On
                RewriteCond %{REQUEST_FILENAME} -f [OR]
                RewriteCond %{REQUEST_FILENAME} !-f
                RewriteRule ^(.*)$ index.php [L,QSA]
                </IfModule>' > /var/www/$domain/.htaccess
                service apache2 restart
            fi
}

function composer(){
#composer
    clear
    echo
    echo "Voulez vous installer composer ? : si oui répondre avec la touche 'Y' "
    read choice
    if [[ "$choice" ==  [yY] ]]; then
        apt update && apt install php7.4-soap php7.4-xml zip php7.4-zip -y
        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
        php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
        php composer-setup.php
        php -r "unlink('composer-setup.php');"
        mv composer.phar /usr/local/bin/composer
    fi
}

function cloner(){
#cloner
clear
echo "Voulez vous cloner un projet ? : si oui répondre avec la touche 'Y' "
    read choice
    if [[ "$choice" ==  [yY] ]]; then
        if [ -z "$domain" ]; then
                    echo "Pour quel domaine ou sous-domaine ? "
                    choice=n
                    while [[ "$choice" ==  [nN] ]]
                    do
                        read domain
                        clear
                        echo
                        echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
                        echo "nom d'utilisateur : $domain "
                        read choice
                    done
        fi
        echo "Pour quel emplacement ? chemin absolue ex : /var/www/$domain "
        choice=n
        while [[ "$choice" ==  [nN] ]]
        do
            read dossier
            clear
            echo
            echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
            echo "nom d'e dossier : $dossier "
            read choice
        done
        mkdir $dossier
        echo "Pour quel est l'url ? ex : https://github.com/pgrimaud/ratp-api-rest.git "
        choice=n
        while [[ "$choice" ==  [nN] ]]
        do
            read url
            clear
            echo
            echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
            echo "url : $url "
            read choice
        done
        git clone $url  $dossier
        if [ -z "$user" ]; then
            echo "Pour quel utilisateur ? "
            choice=n
            while [[ "$choice" ==  [nN] ]]
            do
                    read user
                    clear
                    echo
                    echo " C'est ça ? si vous voulez changer, répondre avec la touche 'N' "
                    echo "nom d'utilisateur : $user "
                    read choice
            done
        fi
        chown -R $user:$user /var/www$domain
        clear
        echo
        echo "Les commandes suivantes on était effectuées :"
        echo
        echo "
            mkdir $dossier
            git clone $url  $dossier
            sed -i \"/DocumentRoot \/var\/www\/$domain/a \
            chown -R $user:$user /var/www$domain"
        echo
        pause 'Presse [Entrer] pour continuer...'
    fi
}

function init(){
  clear
  echo
  echo " Bienvenu  dans InitScript  !!!" 
  echo "Voulez vous commencer ? : si oui répondre avec la touche 'Y' "
  read choice
  if [[ "$choice" ==  [yY] ]]; then
    create_user
    suprootssh
    changeport
    connexionssh
    update
    installphp
    fcgiphp
    apache
    wordpress
    certbot
    fcgisitephp
    htpassword
    htaccess
    composer
    cloner
  fi
}

## ----------------------------------
# #7: Fonction lancé par le script
## ----------------------------------
parse_options $@
